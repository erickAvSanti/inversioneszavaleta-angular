import { Component, OnInit, Input, isDevMode } from '@angular/core'
import { FormGroup, FormControl } from '@angular/forms'
import { UserService } from 'src/app/services/user.service'
import { MatDialog } from '@angular/material/dialog'
import { NamedRoutesService } from './../../../services/routes/named-routes.service'
import { Router, ActivatedRoute, ParamMap } from '@angular/router'
import { finalize, delay } from 'rxjs/operators'
import { IUser } from 'src/app/interfaces/iuser'
import { AuthService } from 'src/app/services/auth.service'
import { of, empty } from 'rxjs'

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.sass']
})
export class UserFormComponent implements OnInit {

  @Input('userId') _id: number = null  
  error_messages: Array<string> = []

  mainForm = new FormGroup({
    full_name: new FormControl(''),
    username: new FormControl(''),
    password: new FormControl(''),
  })
  hide_password: boolean = false

  sync: {wait: boolean, message: string} = {wait: false, message: ''}

  constructor(
    private namedRoutesService: NamedRoutesService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private authService: AuthService,
    public userService: UserService
  ) { }

  ngOnInit(): void {
    this.mainForm.disable()
    this.onRouteParamChange()
  }
  onRouteParamChange(){
    this.route.paramMap.subscribe(
      (paramMap: ParamMap) => {
        this.getUserInfo(paramMap)
      }
    )
    /*
    this.route.paramMap.pipe(
      switchMap((params: ParamMap)=>{
        const tmp_id = paramMap.get('id')
        return this.productService.get(this._id)
      })
    )
    */
  }
  getUserInfo(paramMap: ParamMap){
    const tmp_id = paramMap.get('id')
    if(/^\d+$/.test(tmp_id)){
      this._id = +tmp_id
      this.sync.wait = true
      this.userService.get(this._id).pipe(finalize(()=>{this.sync.wait=false})).subscribe(
        (res : IUser)=>{
          if(isDevMode())console.log(res)
          this.fillForm(res)
          this.mainForm.enable()
        },
        (err : any) => {
          if(isDevMode())console.log(err)
          if(err.status == 404){
            this.sync.message = 'Usuario no encontrado'
          } else if(err.status == 401){
            this.authService.logout()
          }else{
            this.sync.message = 'Error al obtener información del usuario'
          }
          of(1).pipe(delay(1500)).subscribe(
            (res) => {
              this.router.navigate([this.namedRoutesService.getRoute('dashboard.users')])
            }
          )
        }
      )
    }else{
      if(tmp_id !== 'add'){
        this.router.navigate([this.namedRoutesService.getRoute('dashboard.users')])
      }else{
        this.fillForm(null)
        this.mainForm.enable()
      }
    }
  }

  save(){
    if(this.sync.wait)return
    const raw = this.mainForm.getRawValue()
    if(isDevMode()){
      console.log(raw)
    }
    this.sync.wait = true
    this.sync.message = ''
    this.userService.save(raw,this._id).pipe(finalize(()=>{this.sync.wait=false})).subscribe(
      (res)=>{
        if(isDevMode()){
          console.log(res)
        }
        if(res.id){
          this.router.navigate([this.namedRoutesService.getRoute('dashboard.users.form',{id:res.id})])
        }
      },
      (err)=>{
        if(isDevMode()){
          console.log(err)
          console.log(err.error)
        }
        if(err.status == 422){
          this.setErrorMessages(err.error)
        } else if(err.status == 401){
          this.authService.logout()
        }else{
          //TODO
        }
        this.sync.message = 'Error'
      }
    )
  }
  
  fillForm(res:IUser){
    this.mainForm.get('full_name').setValue(res?.full_name ?? '')
    this.mainForm.get('username').setValue(res?.username ?? '')
    this.mainForm.get('password').setValue('')
  }

  setErrorMessages(error: {}){
    this.error_messages = []
    for(const idx in error){
      const arr = error[idx]
      if(arr && arr.length){
        arr.forEach(msg => {
          this.error_messages.push(msg)
        })
      }
    }
    console.log("errores ",this.error_messages)
  }
  removeErrorMessage(msg: string) {
    this.error_messages.splice(this.error_messages.indexOf(msg), 1)
  }

}
