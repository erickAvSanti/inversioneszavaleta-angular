import { Component, OnInit, isDevMode, ViewChild } from '@angular/core'
import { Router } from '@angular/router'

import { NamedRoutesService } from '../../services/routes/named-routes.service'
import { UserService } from '../../services/user.service'
import { finalize, delay } from 'rxjs/operators'
import { of, empty } from 'rxjs'
import { MatPaginatorIntl } from '@angular/material/paginator'

import { FormControl, FormGroup } from '@angular/forms'
import { DialogUserDeleteComponent } from './dialog-user-delete/dialog-user-delete.component'

import { IUser } from '../../interfaces/iuser'
import { MatDialog } from '@angular/material/dialog'

function CustomPaginator() {
  const customPaginatorIntl = new MatPaginatorIntl()

  customPaginatorIntl.itemsPerPageLabel = 'Usuarios x página:'

  return customPaginatorIntl
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass'],
  providers: [
    { provide: MatPaginatorIntl, useValue: CustomPaginator() }
  ]
})
export class UsersComponent implements OnInit {

  

  sync: {wait:boolean, message: string} = {wait: false, message: ''}
  show_main_panel: boolean = true
  records: Array<any> = [] 
  paginator: {records_x_page: number, current_page: number, total_records: number, search: string} = {records_x_page:20, current_page: 1, total_records: 100, search: ''}
  
  formSearch: FormGroup = new FormGroup({
    search: new FormControl('')
  })
  
  constructor(
    private namedRoutesService: NamedRoutesService,
    public router: Router,
    public dialog: MatDialog,
    public userService: UserService
  ) { }

  ngOnInit(): void {
    this.delayGetAll()
  }
  delayGetAll(){
    of(1).pipe(delay(500)).subscribe(()=>this.getAll())
  }
  getAll(){
    if(this.sync.wait)return
    this.sync.wait = true
    this.userService.getAll(this.paginator).pipe(
      finalize(()=>this.sync.wait=false)
    ).subscribe(
      res=>{
        if(isDevMode())console.log(res)
        this.records = res.records
        this.paginator.records_x_page = res.records_x_page
        this.paginator.current_page = res.current_page
        this.paginator.total_records = res.total_records
      },
      err=>{
        if(isDevMode())console.log(err)
        this.records = []
      }
    )
  }

  addAction(){
    this.router.navigate([this.namedRoutesService.getRoute('dashboard.users.form',{id:'add'})])

  }
  reloadAll(){
    this.getAll()
  }
  onActivateRouteOutlet(evt){
    this.show_main_panel = false
  }
  onDeactivateRouteOutlet(evt){
    this.show_main_panel = true
  }
  viewItem(user: IUser){
    this.router.navigate([this.namedRoutesService.getRoute('dashboard.users.form',{id: user.id})])
  }
  onPaginationChange(evt){
    if(isDevMode())console.log(evt)
    if(this.paginator.records_x_page == evt.pageSize){
      this.paginator.current_page = evt.pageIndex + 1
    }else{
      this.paginator.current_page = 1
    }
    this.paginator.records_x_page = evt.pageSize
    this.getAll()
  }
  onInputSearch(text: string){
    if(isDevMode())console.log(text)
    this.paginator.search = text
    this.getAll()
  }
  delete(data:IUser){
    if(isDevMode())console.log('delete user = ',data)
    const dialogRef = this.dialog.open(DialogUserDeleteComponent, {
      width: '100%',
      maxWidth: '500px',
      minWidth: '250px',
      data
    })
    dialogRef.afterClosed().subscribe((result: IUser) => {
      if(isDevMode())console.log(result)
      this.getAll()
    })
  }

}
