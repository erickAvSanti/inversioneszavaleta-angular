import { Component, OnInit, isDevMode, Input, OnDestroy, AfterViewInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AuthService } from '../../services/auth.service'
import { ActivatedRoute, Router } from '@angular/router'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {

  form: FormGroup
  wait_login: boolean
  username_invalid: boolean
  message: string

  @Input() redirect: boolean = true

  private dataSubscription: Subscription

  constructor(
    private fb:FormBuilder,
    private authService: AuthService,
    public router: Router,
    private route: ActivatedRoute
  ) { 

    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    })
    this.onChanges();

  }
  ngAfterViewInit(): void {
    this.subscribeToDataChange()
  }
  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe()
    }
  }
  private subscribeToDataChange(): void {
    this.dataSubscription = this.route.data.subscribe((data: any) => {
      console.log(`subscribeToDataChange`,data)
      if(typeof data.redirect === 'boolean'){
        this.redirect = data.redirect
      }
    });
  }

  ngOnInit(): void {
    if( isDevMode() ){
      console.log(`LoginComponent ngOnInit`)
    }
  }

  onChanges(){
    this.form.valueChanges.subscribe(
      value => {
        if( isDevMode() ){
          console.log(`form valid: ${this.form.valid ? 'si' : 'no'}`)
        }
      }
    )
  }
  get username(){
    return this.form.get('username')
  }
  get password(){
    return this.form.get('password')
  }

  login(){
    const val = this.form.value
    if( isDevMode() ){
      console.log(val)
    }
    if( val.username && val.password ){
      if(this.wait_login)return
      this.wait_login = true
      this.message = ''
      this.authService.login(val.username, val.password)
      .subscribe(
        res => {
          this.wait_login = false
          if(isDevMode()){
            console.log(`User is logged in`)
            console.log(res)
          }
          this.message = ''
          if(this.redirect){
            if(!this.authService.tryToRedirectUrl()){
              this.router.navigate(['dashboard'])
            }
            
          }
        },
        err=>{ 
          if(isDevMode())console.log(err)
          this.wait_login = false
          this.message = 'No se pudo iniciar sesión'
         }
      )
    }
  }
  getUsernameErrorMessage() {
    if (this.username.hasError('required')) {
      return 'usuario requerido'
    }

    return this.username.hasError('email') ? 'usuario no válido' : ''
  }
  getPasswordErrorMessage() {
    if (this.password.hasError('required')) {
      return 'contraseña requerido'
    }
  }

}
