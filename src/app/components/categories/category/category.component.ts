import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { CategoryData } from '../categories.component'

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.sass']
})
export class CategoryComponent implements OnInit {

  @Input('record') prop: CategoryData = {id:null, cat_name: '', children:[]}
  @Input() enableAddAction: boolean = true
  @Output() add = new EventEmitter<void>()
  @Output() edit = new EventEmitter<void>()
  @Output() delete = new EventEmitter<void>()
  constructor() { }

  ngOnInit(): void {
  }
  public addAction(){
    this.add.emit()
  }
  public editAction(){
    console.log('edit emit')
    this.edit.emit()
  }
  public deleteAction(){
    this.delete.emit()
  }

}