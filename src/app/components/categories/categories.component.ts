import { Component, OnInit, Output, EventEmitter, isDevMode } from '@angular/core'
import { DialogEditComponent } from '../categories/dialog-edit/dialog-edit.component'
import { DialogDeleteComponent } from '../categories/dialog-delete/dialog-delete.component'
import { MatDialog } from '@angular/material/dialog'
import { CategoryService } from '../../services/category.service'
import { delay, finalize } from 'rxjs/operators'
import { of } from 'rxjs'

export interface CategoryData {
  id: number
  cat_name: string
  cat_id?: number
  children: Array<CategoryData>
}


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.sass']
})
export class CategoriesComponent implements OnInit {

  wait_sync: boolean = false

  constructor(
    public dialog: MatDialog,
    private categoryService: CategoryService
  ) { }

  public categories: CategoryData[] = [
    {id: 1, cat_name:'c1', children:[]},
    {id: 2, cat_name:'c2', children:[
      {id: 3, cat_name: 'a1', children:[]},
      {id: 4, cat_name: 'a2', children:[
        {id: 5, cat_name:'w1', children:[]},
        {id: 6, cat_name:'w2', children:[]},
        {id: 7, cat_name:'w3', children:[]},
        {id: 8, cat_name:'w4', children:[]},
      ]},
      {id: 9, cat_name: 'a3', children:[]},
      {id: 10, cat_name: 'a4', children:[]},
      {id: 11, cat_name: 'a5', children:[]},
    ]},
    {id: 12, cat_name:'c3', children:[
      {id: 13, cat_name: 'b1', children:[]},
      {id: 14, cat_name: 'b2', children:[]},
      {id: 15, cat_name: 'b3', children:[]},
    ]},
    {id: 16, cat_name:'c4', children:[]},
    {id: 17, cat_name:'c5', children:[
      {id: 18, cat_name: 'd1', children:[]},
      {id: 19, cat_name: 'd2', children:[]},
      {id: 20, cat_name: 'd3', children:[]},
    ]},
    {id: 21, cat_name:'c6', children:[]},
  ]

  ngOnInit(): void {
  }

  editAction(prop: CategoryData):void{
    console.log(prop)
    console.log('edit category component')
    if(this.wait_sync)return
    const dialogRef = this.dialog.open(DialogEditComponent, {
      width: '250px',
      data: {id: prop.id, cat_name: prop.cat_name}
    });

    dialogRef.afterClosed().subscribe((result: CategoryData) => {
      console.log(result)
      if(result && !this.wait_sync){
        this.wait_sync = true
        result.cat_name = result.cat_name.toUpperCase()
        this.categoryService.updateName(result).pipe(finalize(()=>{this.wait_sync = false})).subscribe(
          (res) => {
            if( isDevMode() ){
              console.log(res)
            }
            prop.cat_name = result.cat_name
          },
          (err)=>{
            if( isDevMode() ){
              console.log(err)
            }
          }
        )
      }
    })
  }

  deleteAction(prop: CategoryData, parent?: any):void{
    console.log(prop, parent)
    console.log('delete category component')
    if(this.wait_sync)return
    const dialogRef = this.dialog.open(DialogDeleteComponent, {
      width: '250px',
      data: {id: prop.id, cat_name: prop.cat_name}
    });

    dialogRef.afterClosed().subscribe((result: CategoryData) => {
      console.log(result)
      if(result && !this.wait_sync){
        this.wait_sync = true
        this.categoryService.delete(result).pipe(finalize(()=>{this.wait_sync = false})).subscribe(
          (res) => {
            if( isDevMode() ){
              console.log(res)
            }
            if(parent && parent.children){
              parent.children = parent.children.filter(child => child.id !== prop.id)
            }else{
              this.categories = this.categories.filter(cat => cat.id !== prop.id)
            }
          },
          (err)=>{
            if( isDevMode() ){
              console.log(err)
            }
          }
        )
      }
    })
  }

  addAction(prop?: CategoryData):void{
    this.showAddDialog(prop)
  }
  private showAddDialog(parent?: CategoryData){
    if(this.wait_sync)return
    const dialogRef = this.dialog.open(DialogEditComponent, {
      width: '250px',
      data: {id: null, cat_name: ''}
    });

    dialogRef.afterClosed().subscribe((result: CategoryData) => {
      console.log(result)
      if(result && !this.wait_sync){
        this.wait_sync = true
        result.cat_id = parent?.id
        result.cat_name = result.cat_name.toUpperCase()
        this.categoryService.add(result).pipe(finalize(()=>{this.wait_sync = false})).subscribe(
          (res) => {
            if( isDevMode() ){
              console.log(res)
            }
            this.reloadAllWithDelay(100)
          },
          (err)=>{
            if( isDevMode() ){
              console.log(err)
            }
          }
        )
      }
    })
  }

  ngAfterViewInit(){
    console.log('after view init categories')
    this.reloadAllWithDelay()
  }
  reloadAllWithDelay(delayTime: number = 1000){
    of(1).pipe(delay(delayTime)).subscribe(
      ()=>{
        this.reloadAll()
      }
    )
  }

  reloadAll(){
    if(this.wait_sync)return
    this.wait_sync = true
    this.categoryService.all().pipe(
      delay(100)
    ).subscribe(
      (res) => {
        if(isDevMode()){
          console.log(res)
        }
        this.categories = res
      },
      (err)=>{
        if( isDevMode() ){
          console.log(err)
        }
      },
      ()=>{ this.wait_sync  = false }
    )
  }

  trackByIndex(idx: number, item: any){
    return idx
  }

}
