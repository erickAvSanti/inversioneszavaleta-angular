import { Component, OnInit, Inject } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { CategoryData } from '../categories.component'

@Component({
  selector: 'app-dialog-edit',
  templateUrl: './dialog-edit.component.html',
  styleUrls: ['./dialog-edit.component.sass']
})
export class DialogEditComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CategoryData) {}

  ngOnInit(): void {
    
  }
  cancel(){
    this.dialogRef.close()
  }
}
