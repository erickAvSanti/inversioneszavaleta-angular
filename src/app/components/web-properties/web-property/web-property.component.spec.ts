import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebPropertyComponent } from './web-property.component';

describe('WebPropertyComponent', () => {
  let component: WebPropertyComponent;
  let fixture: ComponentFixture<WebPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
