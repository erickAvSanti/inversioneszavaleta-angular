import { Component, OnInit, Input, isDevMode } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { IWebProperty } from 'src/app/interfaces/iweb-property'
import Swal from 'sweetalert2'
import { WebPropertyService } from '../../../services/web-property.service'
import { finalize } from 'rxjs/operators'

@Component({
  selector: 'app-web-property',
  templateUrl: './web-property.component.html',
  styleUrls: ['./web-property.component.sass']
})
export class WebPropertyComponent implements OnInit {

  @Input() record: IWebProperty
  sync: {wait: boolean, message: string} = {wait: false, message: ''}
  WebPropertyForm = new FormGroup({
    prop_key: new FormControl('',Validators.required),
    prop_value: new FormControl('',Validators.required),
  })
  constructor(
    private webPropertyService: WebPropertyService
  ) { }

  ngOnInit(): void {
    this.prop_key.setValue(this.record.prop_key)
    this.prop_value.setValue(this.record.prop_value)
  }
  get prop_key(){
    return this.WebPropertyForm.get('prop_key')
  }
  get prop_value(){
    return this.WebPropertyForm.get('prop_value')
  }
  update(evt): void{
    if(this.sync.wait)return
    if(!this.WebPropertyForm.valid){
      Swal.fire({title:"Rellene todos los campos por favor.", icon: 'info'})
    }
    this.sync.wait = true
    this.sync.message = 'Espere por favor...'
    this.webPropertyService.update(this.record.id,this.WebPropertyForm.getRawValue())
    .pipe(finalize(()=>{this.sync.wait = false}))
    .subscribe(
      (res)=>{
        if(isDevMode())console.log(res)
        this.sync.message = ''
      },
      (err)=>{
        if(isDevMode())console.log(err)
        this.sync.message = 'Error'
      }
    )
    
  }

}
