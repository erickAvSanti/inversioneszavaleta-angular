import { Component, OnInit, isDevMode, Inject } from '@angular/core'
import { delay, finalize } from 'rxjs/operators'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { WebPropertyService } from 'src/app/services/web-property.service'
import { IWebProperty } from 'src/app/interfaces/iweb-property'

import { ISync } from './../../../interfaces/isync'

@Component({
  selector: 'app-dialog-web-property',
  templateUrl: './dialog-web-property.component.html',
  styleUrls: ['./dialog-web-property.component.sass']
})
export class DialogWebPropertyComponent implements OnInit {

  sync: ISync = {wait: false, message: ''}
  error_messages: Array<string> = []
 
  constructor(
    public dialogRef: MatDialogRef<DialogWebPropertyComponent>,
    public webPropertyService: WebPropertyService,
    @Inject(MAT_DIALOG_DATA) public data: IWebProperty) {}

  ngOnInit(): void {
    
  }
  process(){
    if(this.sync.wait)return
    this.sync.wait = true
    this.webPropertyService.create(this.data).pipe(
      delay(400),
      finalize(()=>{this.sync.wait = false})
    ).subscribe(
      (res) => {
        if( isDevMode() )console.log(res)
        this.cancel(this.data)
      },
      (err)=>{
        if( isDevMode() )console.log(err)
        this.error_messages = this.getErrorMessages(err.error)
      }
    )
  }
  cancel(result?: any){
    this.dialogRef.close(result)
  }
  
  createListErrorMessages(arr:Array<string>):string{
    let str = `<ul style='text-align:left;list-style-type:none'>`
    arr.forEach(prop => str += `<li>${prop}</li>`)
    str += '</ul>'
    return str
  }
  getErrorMessages(err:any){
    const arr = []
    if(err?.length>0){
      for(const idx in err){
        const values = err[idx]
        if(values?.length>0 && values.forEach){
          values.forEach(value => arr.push(value))
        }
      }
    }
    if(arr.length == 0)arr.push('Error')
    return arr
  }
  removeErrorMessage(msg: string) {
    this.error_messages.splice(this.error_messages.indexOf(msg), 1)
  }
}
