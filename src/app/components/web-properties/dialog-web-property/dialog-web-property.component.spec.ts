import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogWebPropertyComponent } from './dialog-web-property.component';

describe('DialogWebPropertyComponent', () => {
  let component: DialogWebPropertyComponent;
  let fixture: ComponentFixture<DialogWebPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogWebPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogWebPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
