import { Component, OnInit, AfterViewInit, isDevMode } from '@angular/core'
import {IWebProperty} from '../../interfaces/iweb-property'
import { Observable, Subject, of, timer } from 'rxjs'
import { WebPropertyService } from '../../services/web-property.service'
import { tap, switchMap, finalize, delay } from 'rxjs/operators'
import { DialogWebPropertyComponent } from './dialog-web-property/dialog-web-property.component'
import { MatDialog } from '@angular/material/dialog'

@Component({
  selector: 'app-web-properties',
  templateUrl: './web-properties.component.html',
  styleUrls: ['./web-properties.component.sass']
})
export class WebPropertiesComponent implements OnInit, AfterViewInit {

  web_properties$: Observable<Array<IWebProperty>>
  sync: {wait: boolean} = {wait: false}
  private subjectRequest = new Subject<any>()
  constructor(
    public dialog: MatDialog,
    private webPropertyService: WebPropertyService
  ) { }

  ngOnInit(): void {
    this.web_properties$ = this.subjectRequest.pipe(
      switchMap(_ => this.webPropertyService.all().pipe(finalize(()=>{this.sync.wait = false}))),
      tap(data=>{if(isDevMode())console.log(data)}),
    )
  }
  ngAfterViewInit(){
    this.getDelayDataFromServer()
  }
  getDelayDataFromServer(){
    timer(200).subscribe(res=>{this.getDataFromServer()})
  }
  getDataFromServer(){
    if(this.sync.wait)return
    this.sync.wait = true
    this.subjectRequest.next()
  }
  add(evt: any): void{
    this.showForm({prop_key: '', prop_value: ''} as IWebProperty)
  }
  
  showForm(data: IWebProperty){
    if(this.sync.wait)return
    const dialogRef = this.dialog.open(DialogWebPropertyComponent, {
      width: '100%',
      maxWidth: '500px',
      minWidth: '250px',
      data
    })
    dialogRef.afterClosed().subscribe((result: IWebProperty) => {
      if(isDevMode())console.log(result)
      if(result)this.getDataFromServer()
    })
  }

}
