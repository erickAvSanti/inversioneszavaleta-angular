import { Component, OnInit, Input, isDevMode } from '@angular/core'
import { FormGroup, FormControl } from '@angular/forms'
import { of, empty } from 'rxjs'
import { delay, finalize, map, switchMap } from 'rxjs/operators'

import { NamedRoutesService } from '../../../services/routes/named-routes.service'
import { Router, ActivatedRoute, ParamMap } from '@angular/router'
import { ProductService } from '../../../services/product.service'
import { AuthService } from 'src/app/services/auth.service'
import { CategoryService } from 'src/app/services/category.service'

import { ProductCategory } from '../../../models/product-category'
import { ProductImage } from '../../../models/product-image'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.sass']
})
export class ProductFormComponent implements OnInit {

  @Input('productId') _id: number = null  
  error_messages: Array<string> = []
  categories: Array<ProductCategory> = []

  images_to_upload: Array<File> = []
  images_saved: Array<ProductImage> = []
  current_selected_categories_ids: Array<number> = []

  mainForm = new FormGroup({
    prod_name: new FormControl(''),
    prod_desc: new FormControl(''),
    prod_price: new FormControl(''),
    prod_price_off: new FormControl(''),
    show_prod_price_off: new FormControl(),
  })

  wait_save: boolean = false
  wait_message: string = ''
  sync_categories = {wait: false, message: ''}
  sync_images = {
    wait: false, message: '',
    wait_delete: false,
    wait_set_default: false,
  }
  constructor(
    private namedRoutesService: NamedRoutesService,
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService,
    private authService: AuthService,
    private categoryService: CategoryService,
  ) { 
    /*
    this.router.events.subscribe(
      (evt)=>{
        if(evt instanceof ActivationStart){
          const id = evt.snapshot.paramMap.get('id')
          console.log(`>>>> form id = ${id}`)
        }else{
          console.log('event ',evt)
        }
      }
    )
    */
  }

  ngOnInit(): void {
    this.mainForm.disable()
    this.onRouteParamChange()
  }
  onRouteParamChange(){
    /*
    this.route.paramMap.subscribe(
      (paramMap: ParamMap) => {
        this.getProductInfo(paramMap)
      }
    )
    */
    this.route.paramMap.subscribe(
      (paramMap: ParamMap)=>{
        const tmp_id = paramMap.get('id')
        if(isDevMode()){
          console.log(`this.route.paramMap.subscribe => ${tmp_id}`)
        }
        this.clearProps()
        this.getProductInfo(paramMap)
      },

      err=>{
        this.clearProps()
        if(isDevMode()){
          console.log(`this.route.paramMap.subscribe error`)
          console.log(err)
        }
      }
    )
  }

  clearProps(){
    this.error_messages.splice(0,this.error_messages.length)
    this.categories.splice(0,this.categories.length)
    this.images_to_upload.splice(0,this.images_to_upload.length)
    this.images_saved.splice(0,this.images_saved.length)
    this.current_selected_categories_ids.splice(0,this.current_selected_categories_ids.length)
  }
  getProductInfo(paramMap: ParamMap){
    const tmp_id = paramMap.get('id')
    if(/^\d+$/.test(tmp_id)){
      this._id = +tmp_id
      this.wait_save = true
      this.productService.get(this._id).pipe(finalize(()=>{this.wait_save=false})).subscribe(
        (res : any)=>{
          if(isDevMode())console.log(res)
          this.current_selected_categories_ids = res.categories_ids
          this.fillForm(res)
          this.mainForm.enable()
          this.delayGetCategories()
          this.delayGetImages()
        },
        (err : any) => {
          if(isDevMode())console.log(err)
          if(err.status == 404){
            this.wait_message = 'Producto no encontrado'
          } else if(err.status == 401){
            this.authService.logout()
          }else{
            this.wait_message = 'Error al obtener información del producto'
          }
          of(1).pipe(delay(1500)).subscribe(
            (res) => {
              this.router.navigate([this.namedRoutesService.getRoute('dashboard.products')])
            }
          )
        }
      )
    }else{
      if(tmp_id !== 'add'){
        this.router.navigate([this.namedRoutesService.getRoute('dashboard.products')])
      }else{
        this._id = null
        this.fillForm(null)
        this.mainForm.enable()
      }
    }
  }

  save(){
    if(this.wait_save)return
    const raw = this.mainForm.getRawValue()
    if(isDevMode()){
      console.log(raw)
    }
    this.wait_save = true
    this.wait_message = ''
    this.productService.save(raw,this._id).pipe(finalize(()=>{this.wait_save=false})).subscribe(
      (res)=>{
        if(isDevMode()){
          console.log(res)
        }
        if(res.id){
          this.router.navigate([this.namedRoutesService.getRoute('dashboard.products.form',{id:res.id})])
        }
      },
      (err)=>{
        if(isDevMode()){
          console.log(err)
          console.log(err.error)
        }
        if(err.status == 422){
          this.setErrorMessages(err.error)
        } else if(err.status == 401){
          this.authService.logout()
        }else{
          //TODO
        }
        this.wait_message = 'Error'
      }
    )
  }
  fillForm(res:any){
    this.mainForm.get('prod_name').setValue(res?.prod_name ?? '')
    this.mainForm.get('prod_desc').setValue(res?.prod_desc ?? '')
    this.mainForm.get('prod_price').setValue(res?.prod_price ?? 0)
    this.mainForm.get('prod_price_off').setValue(res?.prod_price_off ?? 0)
    this.mainForm.get('show_prod_price_off').setValue(res && typeof res.show_prod_price_off == 'boolean' ? res.show_prod_price_off : false)
  }

  getCategories(){
    this.categoryService.all().subscribe(
      (res)=>{
        if(isDevMode())console.log(res)
        this.categories = this.addPropsToCategories(res,3,this.current_selected_categories_ids)
      },
      err=>{
        if(isDevMode())console.log(err)
        if(err.status == 401){
          this.authService.logout()
        }
      }
    )
  }

  setErrorMessages(error: {}){
    this.error_messages = []
    for(const idx in error){
      const arr = error[idx]
      if(arr && arr.length){
        arr.forEach(msg => {
          this.error_messages.push(msg)
        })
      }
    }
    console.log("errores ",this.error_messages)
  }
  removeErrorMessage(msg: string) {
    this.error_messages.splice(this.error_messages.indexOf(msg), 1)
  }

  addPropsToCategories(categories: any[], deep_level: number = 3, current_selected_ids?: Array<number>): Array<ProductCategory>{
    const arr = []
    if(deep_level<0)return []
    categories.forEach(category => {
      const prodCategory = new ProductCategory(category.id, category.cat_name, category.cat_id, false, category.children || []) 
      if(current_selected_ids){
        prodCategory.selected = current_selected_ids.includes(category.id)
      }
      if(category?.children?.length>0){
        prodCategory.children = this.addPropsToCategories(category.children,deep_level - 1,current_selected_ids)
      }
      arr.push(prodCategory)
    })
    return arr
  }
  trackByIndex(idx: number, item: any){
    return idx
  }

  updateCategories(){
    if(this.sync_categories.wait)return
    this.sync_categories.wait = true
    this.sync_categories.message = 'Espere por favor...'
    const categories_ids = {}
    ProductCategory.getIdsFromSelectedCategories(this.categories, categories_ids)
    if(isDevMode()){
      console.log("categories_ids: ",categories_ids)
    }
    this.productService.updateCategories(this._id, Object.keys(categories_ids))
    .pipe(finalize(()=>{this.sync_categories.wait=false}))
    .subscribe(
      res=>{
        if(isDevMode())console.log(res)
        this.sync_categories.message=''
      },
      err=>{
        if(isDevMode())console.log(err)
        this.sync_categories.message='Error'
      }
    )
  }
  openTabCategories(){
    window.open(this.namedRoutesService.getRoute('dashboard.categories'))
  }

  onSelectImage(evt){
    this.images_to_upload.push(...evt.addedFiles)
  }
  onRemoveImage(evt){
    this.images_to_upload.splice(this.images_to_upload.indexOf(evt), 1)
  }

  updateImages(){
    if(this.sync_images.wait)return
    this.sync_images.wait = true
    this.productService.updateImages(this._id, this.images_to_upload)
    .pipe(finalize(()=>{this.sync_images.wait=false}))
    .subscribe(
      res=>{
        if(isDevMode())console.log(res)
        this.sync_images.message = ''
        this.delayGetImages()
        this.clearImagestoUpload()
      },
      err=>{
        if(isDevMode())console.log(err)
        this.sync_images.message = 'Error'
      }
    )
  }
  clearImagestoUpload(){
    this.images_to_upload.splice(0,this.images_to_upload.length)
  }
  delayGetImages(){
    of(1).pipe(delay(500)).subscribe(()=>this.getImages())
  }
  delayGetCategories(){    
    of(1).pipe(delay(400)).subscribe(()=>this.getCategories())
  }
  getImages(){
    if(!this._id)return
    if(this.sync_images.wait)return
    this.sync_images.wait = true
    this.productService.getImages(this._id)
    .pipe(
      finalize( () => this.sync_images.wait = false ),
      map( (props: Array<{}>) => props.map( obj=>new ProductImage(obj) ))
    ).subscribe(
      res=>{
        if(isDevMode())console.log(res)
        this.sync_images.message = ''
        this.images_saved = res
      },
      err=>{
        if(isDevMode())console.log(err)
        this.sync_images.message = 'Error'
      }
    )
  }
  setDefaultCatalogWebImage(evt: any,img: ProductImage): void{
    if(isDevMode())console.log(evt,img)
    if(evt === true){
      if(this.sync_images.wait_set_default)return
      this.sync_images.wait_set_default = true
      Swal.fire({
        title:'Espere por favor...',
        icon: 'info'
      })
      this.productService.setDefaultImage(this._id,img).pipe(
        finalize(()=> this.sync_images.wait_set_default = false )
      ).subscribe(
        res=>{
          if(isDevMode())console.log(res)
          Swal.close()
          Swal.fire({title:'Imagen configurada por defecto',icon:'success'})
          this.delayGetImages()
        },
        err=>{
          if(isDevMode())console.log(err)
          Swal.close()
          Swal.fire({title:'No se pudo establecer la imagen por defecto',icon:'error'})
        }
      )
    }
  }
  removeImageFromServer(evt: any,img: ProductImage){
    if(isDevMode())console.log(evt,img)
    if(evt === true){
      if(this.sync_images.wait_delete)return
      this.sync_images.wait_delete = true
      Swal.fire({
        title:'Espere por favor...',
        icon: 'info'
      })
      this.productService.removeImage(this._id,img).pipe(
        finalize(()=> this.sync_images.wait_delete = false )
      ).subscribe(
        res=>{
          if(isDevMode())console.log(res)
          this.images_saved.splice(this.images_saved.indexOf(img), 1)
          Swal.close()
          Swal.fire({title:'Imagen eliminada',icon:'success'})
        },
        err=>{
          if(isDevMode())console.log(err)
          Swal.close()
          Swal.fire({title:'No se pudo eliminar la imagen',icon:'error'})
        }
      )
    }
  }

  gotoNewProductForm(evt: any){
    if(isDevMode()){
      console.log(`gotoNewProductForm`)
      console.log(evt)
    }
    this.router.navigate([this.namedRoutesService.getRoute('dashboard.products.form',{id: 'add'})])
  }

}


