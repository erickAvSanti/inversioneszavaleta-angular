import { Component, OnInit, Input, isDevMode } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { ProductTagDialogFormComponent } from './product-tag-dialog-form/product-tag-dialog-form.component'
import { finalize, delay, map } from 'rxjs/operators'
import { ProductService } from 'src/app/services/product.service'
import { ProductTag } from 'src/app/models/product-tag'
//import Swal from 'sweetalert2'
import { of, empty } from 'rxjs'

@Component({
  selector: 'app-product-tags',
  templateUrl: './product-tags.component.html',
  styleUrls: ['./product-tags.component.sass']
})
export class ProductTagsComponent implements OnInit {

  @Input() product_id !: number
  sync: {wait:boolean, message: string} = {wait: false, message: ''}

  product_tags: Array<ProductTag> = []

  constructor(
    public dialog: MatDialog,
    public productService: ProductService
    ) { }

  ngOnInit(): void {
    this.delayGetAll()
  }
  addForm(){
    const data: ProductTag = ProductTag.new_default()
    data.product_id = this.product_id
    this.showForm(data)
  }
  editForm(data: ProductTag){
    this.showForm(data)
  }
  deleteForm(data: ProductTag){
    data = data.clone()
    data.mark_to_delete = true
    this.showForm(data)
  }
  reloadAll(){
    this.getAll()
  }
  delayGetAll(){
    of(1).pipe(delay(500)).subscribe(()=>this.getAll())
  }
  getAll(){
    if(this.sync.wait)return
    this.sync.wait = true
    this.productService.getTags(this.product_id).pipe(
      finalize(()=>{this.sync.wait = false}),
      map((res:Array<any>) => res.map(element => ProductTag.new_with(element)))
    ).subscribe(
      (res: Array<ProductTag>) => {
        if( isDevMode )console.log(res)
        this.product_tags = res
      },
      (err)=>{
        if( isDevMode() )console.log(err)
        this.product_tags = []
      }
    )
  }
  showForm(data: ProductTag){
    data = data.clone()
    data.created_at = data.updated_at = null
    if(this.sync.wait)return
    const dialogRef = this.dialog.open(ProductTagDialogFormComponent, {
      width: '100%',
      maxWidth: '500px',
      minWidth: '250px',
      data
    })
    dialogRef.afterClosed().subscribe((result: ProductTag) => {
      if(isDevMode())console.log(result)
      if(result)this.getAll()
    })
    /*

    dialogRef.afterClosed().subscribe((result: ProductTag) => {
      if(isDevMode())console.log(result)
      if(result && !this.sync.wait){
        this.sync.wait = true
        Swal.fire({title:'Procesando...',icon:'info'})
        this.productService.processTag(result).pipe(
          delay(400),
          finalize(()=>{this.sync.wait = false})
        ).subscribe(
          (res) => {
            if( isDevMode )console.log(res)
            Swal.close()
            Swal.fire({title:'Etiqueta creada',icon:'success'})
          },
          (err)=>{
            if( isDevMode() )console.log(err)
            Swal.close()
            const messages = this.getErrorMessages(err.error)
            if(messages?.length>0){
              Swal.fire({html:this.createListErrorMessages(messages),icon:'error'})
            }else{
              Swal.fire({title:'Etiqueta no creada',icon:'error'})
            }
          }
        )
      }
    })
    */
  }
}
