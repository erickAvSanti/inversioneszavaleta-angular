import { Component, OnInit, Inject, isDevMode } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { ProductTag } from '../../../../../models/product-tag'
import { ProductService } from 'src/app/services/product.service'
import { delay, finalize } from 'rxjs/operators'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-product-tag-dialog-form',
  templateUrl: './product-tag-dialog-form.component.html',
  styleUrls: ['./product-tag-dialog-form.component.sass']
})
export class ProductTagDialogFormComponent implements OnInit {

  /*
  constructor(
    public dialogRef: MatDialogRef<ProductTagDialogFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProductTag) {}
  */
  sync: {wait:boolean, message: string} = {wait: false, message: ''}
  error_messages: Array<string> = []
 
  constructor(
    public dialogRef: MatDialogRef<ProductTagDialogFormComponent>,
    public productService: ProductService,
    @Inject(MAT_DIALOG_DATA) public data: ProductTag) {}

  ngOnInit(): void {
    
  }
  process(){
    if(this.sync.wait)return
    this.sync.wait = true
    this.productService.processTag(this.data).pipe(
      delay(400),
      finalize(()=>{this.sync.wait = false})
    ).subscribe(
      (res) => {
        if( isDevMode() )console.log(res)
        this.cancel(this.data)
      },
      (err)=>{
        if( isDevMode() )console.log(err)
        this.error_messages = this.getErrorMessages(err.error)
      }
    )
  }
  cancel(result?: any){
    this.dialogRef.close(result)
  }
  
  createListErrorMessages(arr:Array<string>):string{
    let str = `<ul style='text-align:left;list-style-type:none'>`
    arr.forEach(prop => str += `<li>${prop}</li>`)
    str += '</ul>'
    return str
  }
  getErrorMessages(err:any){
    const arr = []
    if(err?.length>0){
      for(const idx in err){
        const values = err[idx]
        if(values?.length>0 && values.forEach){
          values.forEach(value => arr.push(value))
        }
      }
    }
    if(arr.length == 0)arr.push('Error')
    return arr
  }
  removeErrorMessage(msg: string) {
    this.error_messages.splice(this.error_messages.indexOf(msg), 1)
  }
}
