import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTagDialogFormComponent } from './product-tag-dialog-form.component';

describe('ProductTagDialogFormComponent', () => {
  let component: ProductTagDialogFormComponent;
  let fixture: ComponentFixture<ProductTagDialogFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTagDialogFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTagDialogFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
