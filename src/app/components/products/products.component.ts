import { Component, OnInit, isDevMode, ViewChild } from '@angular/core'
import { Router } from '@angular/router'

import { NamedRoutesService } from '../../services/routes/named-routes.service'
import { ProductService } from '../../services/product.service'
import { finalize, delay } from 'rxjs/operators'
import { of } from 'rxjs'
import { MatPaginatorIntl } from '@angular/material/paginator'

import { FormControl, FormGroup } from '@angular/forms'
import { DialogProductDeleteComponent } from './dialog-product-delete/dialog-product-delete.component'

import { IProduct } from '../../interfaces/iproduct'
import { MatDialog } from '@angular/material/dialog'

function CustomPaginator() {
  const customPaginatorIntl = new MatPaginatorIntl()

  customPaginatorIntl.itemsPerPageLabel = 'Productos x página:'

  return customPaginatorIntl
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.sass'],
  providers: [
    { provide: MatPaginatorIntl, useValue: CustomPaginator() }
  ]
})
export class ProductsComponent implements OnInit {

  

  sync: {wait:boolean, message: string} = {wait: false, message: ''}
  show_main_panel: boolean = true
  products: Array<any> = [] 
  paginator: {records_x_page: number, current_page: number, total_records: number, search: string} = {records_x_page:20, current_page: 1, total_records: 100, search: ''}
  
  formSearch: FormGroup = new FormGroup({
    search: new FormControl('')
  })
  
  constructor(
    private namedRoutesService: NamedRoutesService,
    public router: Router,
    public dialog: MatDialog,
    public productService: ProductService
  ) { }

  ngOnInit(): void {
    this.delayGetAll()
  }
  delayGetAll(){
    of(1).pipe(delay(500)).subscribe(()=>this.getAll())
  }
  getAll(){
    if(this.sync.wait)return
    this.sync.wait = true
    this.productService.getAll(this.paginator).pipe(
      finalize(()=>this.sync.wait=false)
    ).subscribe(
      res=>{
        if(isDevMode())console.log(res)
        this.products = res.records
        this.paginator.records_x_page = res.records_x_page
        this.paginator.current_page = res.current_page
        this.paginator.total_records = res.total_records
      },
      err=>{
        if(isDevMode())console.log(err)
        this.products = []
      }
    )
  }

  addAction(){
    this.router.navigate([this.namedRoutesService.getRoute('dashboard.products.form',{id:'add'})])

  }
  reloadAll(){
    this.getAll()
  }
  onActivateRouteOutlet(evt){
    this.show_main_panel = false
    if(isDevMode()){
      console.log(`products: onActivateRouteOutlet`)
      console.log(evt)
    }
  }
  onDeactivateRouteOutlet(evt){
    this.show_main_panel = true
    if(isDevMode()){
      console.log(`products: onDeactivateRouteOutlet`)
      console.log(evt)
    }
    this.delayGetAll()
  }
  viewItem(product: any){
    this.router.navigate([this.namedRoutesService.getRoute('dashboard.products.form',{id: product.id})])
  }
  onPaginationChange(evt){
    if(isDevMode())console.log(evt)
    if(this.paginator.records_x_page == evt.pageSize){
      this.paginator.current_page = evt.pageIndex + 1
    }else{
      this.paginator.current_page = 1
    }
    this.paginator.records_x_page = evt.pageSize
    this.getAll()
  }
  onInputSearch(text: string){
    if(isDevMode())console.log(text)
    this.paginator.search = text
    this.getAll()
  }
  delete(data:IProduct){
    if(isDevMode())console.log('delete product = ',data)
    const dialogRef = this.dialog.open(DialogProductDeleteComponent, {
      width: '100%',
      maxWidth: '500px',
      minWidth: '250px',
      data
    })
    dialogRef.afterClosed().subscribe((result: IProduct) => {
      if(isDevMode())console.log(result)
      this.getAll()
    })
  }

}
