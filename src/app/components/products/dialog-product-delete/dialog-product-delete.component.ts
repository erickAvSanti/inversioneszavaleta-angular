import { Component, OnInit, Inject, isDevMode } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { ProductService } from 'src/app/services/product.service'
import { ProductTag } from 'src/app/models/product-tag'
import { IProduct } from 'src/app/interfaces/iproduct'
import { finalize } from 'rxjs/operators'
import { MatSlideToggleChange } from '@angular/material/slide-toggle'

@Component({
  selector: 'app-dialog-product-delete',
  templateUrl: './dialog-product-delete.component.html',
  styleUrls: ['./dialog-product-delete.component.sass']
})
export class DialogProductDeleteComponent implements OnInit {
  sync: {wait:boolean, message: string} = {wait: false, message: ''}
  error_messages: Array<string> = []
  forceDelete: boolean = false
  constructor(
    public dialogRef: MatDialogRef<DialogProductDeleteComponent>,
    public productService: ProductService,
    @Inject(MAT_DIALOG_DATA) public data: IProduct) {}

  ngOnInit(): void {
    
  }
  process(){
    if(this.sync.wait)return
    this.sync.wait = true
    this.sync.message = ''
    if(this.forceDelete){
      this.data.force_delete = true
    }else{
      this.data.force_delete = null
    }
    if(isDevMode()){
      console.log('Eliminando...',this.data)
    }
    this.productService.delete(this.data).pipe(
      finalize(()=>this.sync.wait = false)
    ).subscribe(
      res=>{
        if(isDevMode())console.log(res)
        this.sync.message = ''
        this.cancel(this.data)
      },
      err=>{
        if(isDevMode())console.log(err)
        this.sync.message = err.error.message ?? 'Error'
      }
    )
  }
  cancel(result?: IProduct){
    this.dialogRef.close(result)
  }
  onToggleChange(evt: MatSlideToggleChange){
    if(isDevMode())console.log(evt)
    this.forceDelete = evt.checked
  }

}
