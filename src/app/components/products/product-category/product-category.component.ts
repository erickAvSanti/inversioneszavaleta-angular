import { Component, OnInit, Input } from '@angular/core'
import { ProductCategory } from '../../../models/product-category'

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.sass']
})
export class ProductCategoryComponent implements OnInit {

  @Input('record') category: ProductCategory = null
  constructor() { }

  ngOnInit(): void {
  }
  toggleChildrenSelection():void{
    if(this.category?.children?.length>0){
      if(this.category.isAllChildrenSelected()){
        this.category.unSelectAllChildren()
      }else if(this.category.isAtLeastOneChildSelected()){
        this.category.selectAllChildren()
      }else{
        this.category.selectAllChildren()
      }
    }else{
      this.category.selected = !this.category.selected
    }
  }

}
