import { Component, OnInit, ViewChild, HostListener, isDevMode } from '@angular/core'
import { MatDialog, MatDialogConfig } from '@angular/material/dialog'
import { _MatMenu } from '@angular/material/menu'
import { DialogLoginComponent } from '../dialogs/dialog-login/dialog-login.component'

import { NamedRoutesService } from '../../services/routes/named-routes.service'
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  //public getRoute: (...any) => string
  private is_nav_collapsed: boolean = false
  show_main_panel: boolean = true
  @ViewChild('navMenuLeft') navMenuLeft: any
  constructor(
    public dialog: MatDialog,
    public namedRoutesService: NamedRoutesService,
    private authService: AuthService,
    public router: Router,
  ) { 
    //this.getRoute = namedRoutesService.getRoute.bind(namedRoutesService)
  }
  ngAfterViewInit() {
    this.hideNavMenuLeftOpened()
    this.setNavCollapsed()
  }
  setNavCollapsed(){
    if(window.innerWidth < 750){
      this.is_nav_collapsed = true
    }
  }

  @HostListener('window:resize', ['$event'])
  onWindowResize(evt: Event){
    this.setNavCollapsed()
  }


  hideNavMenuLeftOpened(){
    const ul = this.navMenuLeft.nativeElement
    if(isDevMode())console.log(ul.children)
    for(const li of ul.children){
      if(li.childElementCount > 1){
        const li_children = li.children
        for(const li_child of li_children){
          if(li_child.tagName === 'UL'){
            //li_child.style.display = 'none'
          }
        }
      }
    }
  }

  onClickNav(evt: any){
    console.log(evt)
    evt.preventDefault()
    let parent = evt.target
    if(parent.tagName !== 'LI'){
      parent = this.getParent(parent)
      if(parent.childElementCount > 1){
        const ul = parent.getElementsByTagName('UL')[0]
        ul.classList.toggle('show')
      }
    }
    console.log(parent)
  }
  private getParent(element: any, tagName: string = 'LI', counter: number = 5){
    if(element.tagName === tagName) return element
    if(counter<=0)return null
    return this.getParent(element.parentElement,tagName, --counter)
  }

  get navRippleColor(){
    return '#ffffff44'
  }

  get isNavCollapsed(){
    return this.is_nav_collapsed
  }
  toggleMenu(){
    this.is_nav_collapsed = !this.is_nav_collapsed
  }

  numbers(n: number): Array<number>{
    const arr = []
    for(let i = 0; i < n; i++){
      arr.push(i)
    }
    return arr
  }
  ngOnInit(): void {
  }
  openLoginDialog(){
    const dialogConfig = new MatDialogConfig()
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = true
    this.dialog.open( DialogLoginComponent, dialogConfig )
  }
  logout():void{
    this.authService.logout()
  }
  onActivateRouteOutlet(evt){
    this.show_main_panel = false
  }
  onDeactivateRouteOutlet(evt){
    this.show_main_panel = true
  }

}
