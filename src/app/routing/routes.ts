
import { LoginComponent } from '../components/login/login.component'

import { DashboardComponent } from '../components/dashboard/dashboard.component'

import { CanActivateDashboardGuard } from '../guards/can-activate-dashboard.guard'
import { CanActivateLoginGuard } from '../guards/can-activate-login.guard'

import { CategoriesComponent } from '../components/categories/categories.component'
import { UsersComponent } from '../components/users/users.component'
import { ProductsComponent } from '../components/products/products.component'
import { ProductFormComponent } from '../components/products/product-form/product-form.component'
import { UserFormComponent } from '../components/users/user-form/user-form.component'
import { PageNotFoundComponent } from '../components/page-not-found/page-not-found.component'
import { WebPropertiesComponent } from '../components/web-properties/web-properties.component'


export const DEFAULT_ROUTES = [
    { path: '', pathMatch: 'full', redirectTo: 'dashboard' }, 
    { path: 'login', component: LoginComponent, name: 'login', canActivate: [CanActivateLoginGuard] },
    { path: 'dashboard', component: DashboardComponent, name: 'dashboard', canActivate: [CanActivateDashboardGuard], children: [
        { path: 'categories', component: CategoriesComponent, name: 'dashboard.categories' },
        { path: 'users', component: UsersComponent, name: 'dashboard.users', children: [
            { path: 'form/:id', component: UserFormComponent, name: 'dashboard.users.form' },
            ], 
        },
        { path: 'products', component: ProductsComponent, name: 'dashboard.products', children: [
            { path: 'form/:id', component: ProductFormComponent, name: 'dashboard.products.form' },
            ],
        },
        { path: 'webproperties', component: WebPropertiesComponent, name: 'dashboard.webproperties' },
        
    ],
    /*
    resolve: {
        crisis: RouteCrisisService
    } 
    */
    },
    { path: '**', component: PageNotFoundComponent }
]