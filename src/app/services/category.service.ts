import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { environment } from '../../environments/environment'
import { CategoryData } from '../components/categories/categories.component'

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private defaultHttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept' : 'application/json'
    })
  }

  constructor(
    private http: HttpClient
  ) { }
  updateName({id,cat_name}: CategoryData){

    const data = {id,cat_name}
    return this.http.put<any>(`${environment.SERVER_URL_API}/categories/${id}`,data,this.defaultHttpOptions)
  }
  add({cat_name, cat_id}: CategoryData){

    const data = {cat_name, cat_id}
    return this.http.post<any>(`${environment.SERVER_URL_API}/categories`,data,this.defaultHttpOptions)
  }
  delete({id}: CategoryData){

    return this.http.delete<any>(`${environment.SERVER_URL_API}/categories/${id}`)
  }
  all(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept' : 'application/json'
      }),
      params: new HttpParams({})
    }
    return this.http.get<any>(`${environment.SERVER_URL_API}/categories`,httpOptions)
  }
}
