import { Injectable } from '@angular/core'
import{ Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { Observable, of, EMPTY } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class RouteCrisisService implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    // your logic goes here
    return EMPTY
  }
}
