import { TestBed } from '@angular/core/testing';

import { WebPropertyService } from './web-property.service';

describe('WebPropertyService', () => {
  let service: WebPropertyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebPropertyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
