import { Injectable, isDevMode } from '@angular/core'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { environment } from '../../environments/environment'
import { ProductImage } from '../models/product-image'
import { ProductTag } from '../models/product-tag'
import { IProduct } from '../interfaces/iproduct'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private defaultHttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept' : 'application/json'
    })
  }

  constructor(
    private http: HttpClient
  ) { }
  save(props: any, id?: number){
    const data = {...props}
    data.id = id ?? null
    if(id){
      return this.http.put<any>(`${environment.SERVER_URL_API}/products/${id}`,data,this.defaultHttpOptions)
    }else{
      return this.http.post<any>(`${environment.SERVER_URL_API}/products`,data,this.defaultHttpOptions)
    }
  }
  get(id: number){
    return this.http.get<any>(`${environment.SERVER_URL_API}/products/${id}`,this.defaultHttpOptions)
  }
  delete(product: IProduct){

    const params: any = {} 
    if(product.force_delete)params.force_delete = true

    const opts = {
      params
    }
    return this.http.delete<any>(`${environment.SERVER_URL_API}/products/${product.id}`,opts)
  }
  getAll(params:{}){
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept' : 'application/json'
      }),
      params
    }
    if(isDevMode())console.log("opts on getAll = ",opts)
    return this.http.get<any>(`${environment.SERVER_URL_API}/products`,opts)
  }
  updateCategories(id: number, categories: Array<string>){
    const data = {categories}
    return this.http.put<any>(`${environment.SERVER_URL_API}/products/set-categories/${id}`,data,this.defaultHttpOptions)
  }
  updateImages(id: number, images: Array<File>){
    const data: FormData = new FormData()
    images.forEach((imgFile,idx) => {
        data.append(`img${idx}`,imgFile)
    })
    return this.http.put<any>(`${environment.SERVER_URL_API}/products/set-images/${id}`,data)
  }
  getImages( id: number ){
    return this.http.get<any>(`${environment.SERVER_URL_API}/products/images/${id}`,this.defaultHttpOptions)
  }
  removeImage( id: number, img: ProductImage ){
    return this.http.delete<any>(`${environment.SERVER_URL_API}/products/remove-image/${id}/${img.id}`,this.defaultHttpOptions)
  }
  setDefaultImage( id: number, img: ProductImage ){
    return this.http.put<any>(`${environment.SERVER_URL_API}/products/set-default-image/${id}/${img.id}`,this.defaultHttpOptions)
  }
  getTags( id: number ){
    return this.http.get<any>(`${environment.SERVER_URL_API}/product-tags/${id}`,this.defaultHttpOptions)
  }
  processTag( props: ProductTag ){
    if(props.id && props.mark_to_delete){
      return this.http.delete<any>(`${environment.SERVER_URL_API}/product-tags/${props.id}`)
    }else{
      if(props.id){
        return this.http.put<any>(`${environment.SERVER_URL_API}/product-tags/${props.id}`,props, this.defaultHttpOptions)
      }else{
        return this.http.post<any>(`${environment.SERVER_URL_API}/product-tags`,props, this.defaultHttpOptions)
      }
    }
  }
}
