import { Injectable, isDevMode } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { User } from '../models/user'
import { tap, shareReplay } from 'rxjs/operators'
import { environment } from '../../environments/environment'

import { NamedRoutesService } from '../services/routes/named-routes.service'

import * as moment from 'moment'
import * as moment_tz from 'moment-timezone'
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public redirectUrl: string
  constructor(
    private http: HttpClient,
    private router: Router,
    private namedRoutesService: NamedRoutesService
  ) { }
  login(username: string,password: string){

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept' : 'application/json'
      })
    }

    const data = {
      username,
      password
    }
    return this.http.post<User>(`${environment.SERVER_URL_API}/login`,data,httpOptions)
    .pipe(
      tap(res => this.setSession(res)),
      shareReplay()
    ) 
  }
  private setSession(authResult: any ){
    if( isDevMode() ){
      console.log(`authResult: `)
      console.log(authResult)
    }
    const expires_at = moment().utc().add(authResult.expires_in,'hour')
    if( isDevMode() ){
      console.log(`timezone: ${moment_tz.tz.guess(true)}`)
      console.log(expires_at.format('DD/MM/YYYY HH:mm:ss a'))
    }
    localStorage.setItem('token',authResult.token)
    localStorage.setItem('expires_at',JSON.stringify(expires_at.valueOf()))
  }

  logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("expires_at");
    this.router.navigate([this.namedRoutesService.getRoute('login')])
  }

  public isLoggedIn() {
    return moment().utc().isBefore(this.getExpiration())
  }

  isLoggedOut() {
    return !this.isLoggedIn()
  }

  getExpiration() {
    const expiration = localStorage.getItem("expires_at")
    const expires_at = JSON.parse(expiration)
    return moment(expires_at)
  }    
  tryToRedirectUrl():boolean{
    if(this.redirectUrl){
      
      /*
      const navigationExtras: NavigationExtras = {
        queryParamsHandling: 'preserve',
        preserveFragment: true
      }
      this.router.navigate([this.redirectUrl], navigationExtras)
      */
      this.router.navigate([this.redirectUrl])
      this.redirectUrl = null
      return true
    }
    return false
  }
}
