import { Injectable, isDevMode } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../environments/environment'
import { IUser } from '../interfaces/iuser'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private defaultHttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept' : 'application/json'
    })
  }

  constructor(
    private http: HttpClient
  ) { }
  save(props: IUser, id?: number){
    if(id){
      return this.http.put<any>(`${environment.SERVER_URL_API}/users/${id}`,props,this.defaultHttpOptions)
    }else{
      return this.http.post<any>(`${environment.SERVER_URL_API}/users`,props,this.defaultHttpOptions)
    }
  }
  get(id: number){
    return this.http.get<any>(`${environment.SERVER_URL_API}/users/${id}`,this.defaultHttpOptions)
  }
  delete(record: IUser){

    const params: any = {} 
    if(record.force_delete)params.force_delete = true

    const opts = {
      params
    }
    return this.http.delete<any>(`${environment.SERVER_URL_API}/users/${record.id}`,opts)
  }
  getAll(params:{}){
    const opts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept' : 'application/json'
      }),
      params
    }
    if(isDevMode())console.log("opts on getAll = ",opts)
    return this.http.get<any>(`${environment.SERVER_URL_API}/users`,opts)
  }
}
