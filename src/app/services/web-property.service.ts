import { Injectable } from '@angular/core'
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment'
import { IWebProperty } from '../interfaces/iweb-property'

@Injectable({
  providedIn: 'root'
})
export class WebPropertyService {

  
  private defaultHttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept' : 'application/json'
    })
  }

  constructor(
    private http: HttpClient
  ) { }

  
  all(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept' : 'application/json'
      }),
      params: new HttpParams({})
    }
    return this.http.get<any>(`${environment.SERVER_URL_API}/web-properties`,httpOptions)
  }
  update(id: number, values: IWebProperty){
    return this.http.put<any>(`${environment.SERVER_URL_API}/web-properties/${id}`,values,this.defaultHttpOptions)
  }
  create( props: IWebProperty ){
    return this.http.post<any>(`${environment.SERVER_URL_API}/web-properties`,props, this.defaultHttpOptions)
  }

}
