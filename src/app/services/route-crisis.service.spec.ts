import { TestBed } from '@angular/core/testing';

import { RouteCrisisService } from './route-crisis.service';

describe('RouteCrisisService', () => {
  let service: RouteCrisisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RouteCrisisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
