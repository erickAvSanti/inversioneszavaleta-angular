import { Injectable } from '@angular/core'
import { NamedRoutesService as NamedRoutesSrv } from 'ng-named-routes'

@Injectable({
  providedIn: 'root'
})
export class NamedRoutesService {

  constructor(
    private namedRoutesSrv: NamedRoutesSrv
  ) { }

  public getRoute(name: string, extras: {} = {}){
    return `/${this.namedRoutesSrv.getRoute(name,extras)}`
  }
}
