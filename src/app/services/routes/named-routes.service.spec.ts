import { TestBed } from '@angular/core/testing';

import { NamedRoutesService } from './named-routes.service';

describe('NamedRoutesService', () => {
  let service: NamedRoutesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NamedRoutesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
