export interface IProduct {
    id: number
    prod_name: string
    prod_desc: string
    force_delete: boolean 
}
