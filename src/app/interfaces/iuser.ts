export interface IUser {
    id: number
    full_name: string 
    username: string 
    password: string 
    updated_at: string 
    created_at: string
    force_delete: boolean
}
