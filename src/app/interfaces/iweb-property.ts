export interface IWebProperty {
    id: number
    prop_key: string
    prop_desc: string
    prop_value: string
    created_at: string 
    updated_at: string
}
