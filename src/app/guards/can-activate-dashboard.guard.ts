import { Injectable, isDevMode } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, NavigationExtras } from '@angular/router'
import { Observable } from 'rxjs'
import { AuthService } from '../services/auth.service'
import { Router } from '@angular/router'
import { NamedRoutesService } from '../services/routes/named-routes.service'

@Injectable({
  providedIn: 'root'
})
export class CanActivateDashboardGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private namedRoutesService: NamedRoutesService,
  ){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(isDevMode())console.log(`canActivate[url] = ${state.url}`)
    if(!this.authService.isLoggedIn()){
      if(isDevMode()){
        console.log(`No está logueado`)
      }this.authService.redirectUrl = state.url
      /*
      const navigationExtras: NavigationExtras = {
        queryParams: { page_token: Math.random()* 10 ** 10 },
        fragment: 'anchor',
      }
      return this.router.createUrlTree([this.namedRoutesService.getRoute('login')], navigationExtras)
      */
      
      this.router.navigate([this.namedRoutesService.getRoute('login')])
      return false
      
    }
    return true
  }
  
}
