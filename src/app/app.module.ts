import { BrowserModule } from '@angular/platform-browser'
import { NgModule, isDevMode } from '@angular/core'

import { ReactiveFormsModule, FormsModule } from '@angular/forms'

import { AppComponent } from './app.component'
import { LoginComponent } from './components/login/login.component'

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatSliderModule } from '@angular/material/slider'
import {MatButtonModule} from '@angular/material/button'
import {MatInputModule} from '@angular/material/input'
import {MatFormFieldModule} from '@angular/material/form-field'
import {MatCardModule} from '@angular/material/card'
import {MatDialogModule} from '@angular/material/dialog'
import {MatMenuModule} from '@angular/material/menu'
import {MatRippleModule} from '@angular/material/core'
import {MatIconModule} from '@angular/material/icon'
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner'
import {MatProgressBarModule} from '@angular/material/progress-bar'
import {MatCheckboxModule} from '@angular/material/checkbox'
import {MatDividerModule} from '@angular/material/divider'
import {MatPaginatorModule} from '@angular/material/paginator'
import {MatSlideToggleModule} from '@angular/material/slide-toggle'

import { CustomHttpInterceptor } from './interceptors/custom-http.interceptor'

import { DialogLoginComponent } from './components/dialogs/dialog-login/dialog-login.component'

import{ DEFAULT_ROUTES } from './routing/routes'
import { CategoriesComponent } from './components/categories/categories.component'
import { NamedRoutesService } from 'ng-named-routes'
import { Router } from '@angular/router'
import { UsersComponent } from './components/users/users.component'
import { CategoryComponent } from './components/categories/category/category.component'
import { DialogEditComponent } from './components/categories/dialog-edit/dialog-edit.component'
import { DialogDeleteComponent } from './components/categories/dialog-delete/dialog-delete.component'
import { ProductsComponent } from './components/products/products.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { ProductFormComponent } from './components/products/product-form/product-form.component'
import { ProductCategoryComponent } from './components/products/product-category/product-category.component'

import { NgxDropzoneModule } from 'ngx-dropzone'
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2'
import { DateTimeFormatPipe } from './pipes/dates/date-time-format.pipe'
import { ProductTagsComponent } from './components/products/product-form/product-tags/product-tags.component'
import { ProductTagDialogFormComponent } from './components/products/product-form/product-tags/product-tag-dialog-form/product-tag-dialog-form.component'
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component'
import { DialogProductDeleteComponent } from './components/products/dialog-product-delete/dialog-product-delete.component'
import { DialogUserDeleteComponent } from './components/users/dialog-user-delete/dialog-user-delete.component'
import { UserFormComponent } from './components/users/user-form/user-form.component'
import { WebPropertiesComponent } from './components/web-properties/web-properties.component'
import { WebPropertyComponent } from './components/web-properties/web-property/web-property.component'
import { DialogWebPropertyComponent } from './components/web-properties/dialog-web-property/dialog-web-property.component'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    DialogLoginComponent,
    CategoriesComponent,
    UsersComponent,
    CategoryComponent,
    DialogEditComponent,
    DialogDeleteComponent,
    ProductsComponent,
    ProductFormComponent,
    ProductCategoryComponent,
    DateTimeFormatPipe,
    ProductTagsComponent,
    ProductTagDialogFormComponent,
    PageNotFoundComponent,
    DialogProductDeleteComponent,
    DialogUserDeleteComponent,
    UserFormComponent,
    WebPropertiesComponent,
    WebPropertyComponent,
    DialogWebPropertyComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(DEFAULT_ROUTES),
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatDialogModule,
    MatMenuModule,
    MatRippleModule,
    MatIconModule,  
    MatProgressSpinnerModule,  
    MatProgressBarModule, 
    MatCheckboxModule, 
    MatDividerModule, 
    NgxDropzoneModule,
    SweetAlert2Module.forRoot(),
    MatPaginatorModule, 
    MatSlideToggleModule, 
    NgbModule,  
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: CustomHttpInterceptor, multi: true },
    NamedRoutesService,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
  constructor(
    router: Router, namedRoutesService: NamedRoutesService
  ){
    namedRoutesService.setRoutes(router.config)
    
    if(isDevMode()) {
      // Use a custom replacer to display function names in the route configs
      const replacer = (key, value) => (typeof value === 'function') ? value.name : value
      console.log('Routes: ', JSON.stringify(router.config, replacer, 2))
    }

  }
}
