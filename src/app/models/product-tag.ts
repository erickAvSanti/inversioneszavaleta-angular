export class ProductTag {
    public mark_to_delete: boolean = false
	constructor(
        public id: number,
        public product_id: number,
        public tag_key : string,
        public tag_value : string,
        public created_at : string,
        public updated_at : string,
	){
		
    }
    static new_default(){
        return new ProductTag(null,null,'','','','')
    }
    static new_with({id,product_id,tag_key,tag_value,created_at,updated_at}){
        return new ProductTag(id,product_id,tag_key,tag_value,created_at,updated_at)
    }
    public clone(): ProductTag{
        const record: ProductTag = ProductTag.new_with(this)
        if(this.mark_to_delete)record.mark_to_delete = true
        return record
    }
}
