export class ProductCategory{
	constructor(
		public id: number,
		public cat_name: string,
		public cat_id: number,
		public selected: boolean,
		public children: Array<ProductCategory>,
	){
		
	}
	isAllSelected():boolean{
		this.children.forEach(element => {
			if(!element.selected)return false
			if(element.children?.length>0){
				for(const child of element.children){
					if(!child.isAllSelected())return false
				}
			}
		})
		return true
	}
	isAllChildrenSelected():boolean{
		if(this.children?.length>0){
			for(const child of this.children){
				if(child?.children?.length>0){
					for(const child2 of child.children){
						if(!child2.isAllChildrenSelected())return false
					}
				}else{
					if(!child.selected)return false
				}
			}
			return true
		}
		return this.selected
		
	}
	isAtLeastOneChildSelected():boolean{
		if(this.children?.length >0){
			for(const child of this.children){
				if(child?.children?.length>0){
					for(const child2 of child.children){
						if(child2.isAtLeastOneChildSelected())return true
					}
				}else{
					if(child.selected)return true
				}
			}
			return false
		}
		return this.selected
	}
	selectAllChildren():void{
		this.children.forEach(child => {
			child.selected = true
			if(child.children?.length>0){
				child.children.forEach(element => {
					element.selected = true
					element.selectAllChildren()
				})
			}
		})
	}
	unSelectAllChildren():void{
		this.children.forEach(child => {
			child.selected = false
			if(child.children?.length>0){
				child.children.forEach(element => {
					element.selected = false
					element.unSelectAllChildren()
				})
			}
		})
	}

	static getIdsFromSelectedCategories(categories: Array<ProductCategory>, ids:{}): void{
		categories.forEach(category=>{
			if(category.cat_id){//is child
				if(category?.children?.length>0){
					if(category.isAtLeastOneChildSelected()) ids[ category.id ] = category.cat_name
				}else{
					if( category.selected ) ids[ category.id ] = category.cat_name
				}
			}else{//is parent
				if(category.isAtLeastOneChildSelected()) ids[ category.id ] = category.cat_name
			}
			if(category?.children?.length>0){
				ProductCategory.getIdsFromSelectedCategories(category.children,ids)
			}
		})
	}
}