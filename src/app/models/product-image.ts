import { environment } from 'src/environments/environment'

/*
export class ProductImage {
    constructor(
        public id: number|string,
        public file_name: string,
        public _file_hash: string,
        public product_id: number|string,
        public relative_url: string,
        public updated_at: string,
        public created_at: string
    ){}
}
*/
export class ProductImage {
    public id: number|string
    public file_name: string
    public file_hash: string
    public product_id: number|string
    public relative_url: string
    public img_dim_width: number
    public img_dim_height: number
    public is_default: boolean
    public updated_at: string
    public created_at: string
    constructor(props: any){
        this.id = props.id
        this.file_name = props.file_name
        this.file_hash = props.file_hash
        this.product_id = props.product_id
        this.relative_url = props.relative_url
        this.img_dim_width = props.img_dim_width
        this.img_dim_height = props.img_dim_height
        this.is_default = props.is_default
        this.updated_at = props.updated_at
        this.created_at = props.created_at
    }
    public fullUrl(){
        return `${environment.SERVER_URL}/${this.relative_url}`
    }
    public orientation(): string{
        return this.img_dim_width > this.img_dim_height ? 'h':'v'
    }
    public isVertical(){
        return this.orientation() == 'v'
    }
    public isHorizontal(){
        return this.orientation() == 'h'
    }
    public calcDimInBox(box_size: number): Array<number>{
        let ww = 0, hh = 0, values: Array<number> = []

        const img_ratio = this.img_dim_width / this.img_dim_height

        if(box_size > this.img_dim_width){
            ww = box_size
        }else{

        }

        values.push(ww,hh)
        return values
    }
}
