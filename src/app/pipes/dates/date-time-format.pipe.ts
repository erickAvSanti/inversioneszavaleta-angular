import { Pipe, PipeTransform } from '@angular/core'
import * as moment from 'moment'

@Pipe({
  name: 'dateTimeFormat'
})
export class DateTimeFormatPipe implements PipeTransform {

  transform(value: string, format?: string, timezone?: string): string {
    return moment.tz(value,timezone ?? moment.tz.guess()).format(format ?? 'DD/MM/YYYY HH:mm:ss a')
  }

}
